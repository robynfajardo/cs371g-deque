# CS371g: Generic Programming Deque Repo

* Name: Robyn Fajardo, Hogan Tran

* EID: rff359, htt425

* GitLab ID: robynfajardo, hogan900

* Git SHA: 5b5ce90490f895523e0c69b695a441a122a50967

* GitLab Pipelines: https://gitlab.com/robynfajardo/cs371g-deque/-/pipelines

* Estimated completion time: 24 hours

* Actual completion time: 20 hours

* Comments: (any additional comments you have)
