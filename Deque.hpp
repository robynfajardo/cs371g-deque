// -------
// Deque.h
// --------

#ifndef Deque_h
#define Deque_h

// --------
// includes
// --------

#include <algorithm>        // copy, equal, lexicographical_compare, max, swap
#include <cassert>          // assert
#include <initializer_list> // initializer_list
#include <iterator>         // bidirectional_iterator_tag
#include <memory>           // allocator
#include <stdexcept>        // out_of_range
#include <utility>          // !=, <=, >, >=
#include <deque>            // TESTING

// -----
// using
// -----

using std::rel_ops::operator!=;
using std::rel_ops::operator<=;
// -------
using std::rel_ops::operator>;
using std::rel_ops::operator>=;

// destroy
// -------

template <typename A, typename BI>
BI my_destroy (A& a, BI b, BI e) {
    while (b != e) {
        --e;
        a.destroy(&*e);
    }
    return b;
}

// ------------------
// uninitialized_copy
// ------------------

template <typename A, typename II, typename BI>
BI my_uninitialized_copy (A& a, II b, II e, BI x) {
    BI p = x;
    try {
        while (b != e) {
            a.construct(&*x, *b);
            ++b;
            ++x;
        }
    }
    catch (...) {
        my_destroy(a, p, x);
        throw;
    }
    return x;
}

// ------------------
// uninitialized_fill
// ------------------

template <typename A, typename BI, typename T>
void my_uninitialized_fill (A& a, BI b, BI e, const T& v) {
    BI p = b;
    try {
        while (b != e) {
            a.construct(&*b, v);
            ++b;
        }
    }
    catch (...) {
        my_destroy(a, p, b);
        throw;
    }
}

// --------
// my_deque
// --------

template <typename T, typename A = std::allocator<T>>
class my_deque {
    // -----------
    // operator ==
    // -----------

    /**
     * compares the two deques to see if they are the same
     */
    friend bool operator == (const my_deque& lhs, const my_deque& rhs) {
        return (lhs.size() == rhs.size() && std::equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end()));
    }

    // ----------
    // operator <
    // ----------

    /**
     * checks to see if lhs is less than rhs
     */
    friend bool operator < (const my_deque& lhs, const my_deque& rhs) {
        return std::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
    }

    // ----
    // swap
    // ----

    /**
     * swap contents of deque x and deque y
     */
    friend void swap (my_deque& x, my_deque& y) {
        x.swap(y);
    }

public:
    // ------
    // usings
    // ------

    // you must use this allocator for the inner arrays
    using allocator_type  = A;
    using value_type      = typename allocator_type::value_type;

    using size_type       = typename allocator_type::size_type;
    using difference_type = typename allocator_type::difference_type;

    using pointer         = typename allocator_type::pointer;
    using const_pointer   = typename allocator_type::const_pointer;

    using reference       = typename allocator_type::reference;
    using const_reference = typename allocator_type::const_reference;

    // you must use this allocator for the outer array
    using allocator_type_2 = typename A::template rebind<pointer>::other;

private:
    // ----
    // data
    // ----

    allocator_type_2 _outer;
    allocator_type _inner;

    T** outer_begin = nullptr;
    T** outer_end = nullptr;
    size_type inner_begin;
    size_type inner_end;
    size_type len = 0;
    size_type inner_array_len = 10;
private:
    // -----
    // valid
    // -----

    bool valid () const {
        if (outer_begin == nullptr) {
            return false;
        }
        if(inner_end >= inner_begin){
            int cap = std::distance(outer_begin, outer_end) * inner_array_len;
            if(inner_begin <= cap && inner_end <= cap){
                return true;
            }
        }
        return false;
    }

public:
    // --------
    // iterator
    // --------

    class iterator {
        // -----------
        // operator ==
        // -----------

        /**
         * checks if two iterators are equal to one another
         */
        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return (lhs.index == rhs.index) && (rhs.container == lhs.container);
        }

        /**
        * checks if two iterators are not equal to one another
         */
        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

        // ----------
        // operator +
        // ----------

        /**
         * moves pointer rhs places forward
         */
        friend iterator operator + (iterator lhs, difference_type rhs) {
            return lhs += rhs;
        }

        // ----------
        // operator -
        // ----------

        /**
         * moves pointer rhs places backwards
         */
        friend iterator operator - (iterator lhs, difference_type rhs) {
            return lhs -= rhs;
        }

    public:
        // ------
        // usings
        // ------

        // this requires a weaker iterator than the real deque provides
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type        = typename my_deque::value_type;
        using difference_type   = typename my_deque::difference_type;
        using pointer           = typename my_deque::pointer;
        using reference         = typename my_deque::reference;

    private:
        // ----
        // data
        // ----
        my_deque* container;
        size_t index = 0;
        
    private:
        // -----
        // valid
        // -----

        bool valid () const {
            return index >= 0 && (index <= container->size()) && container->valid();
        }

    public:
        // -----------
        // constructor
        // -----------

        /**
         * iterator constructors
         */
        iterator (my_deque* c, size_t pos) {
            container = c;
            index = pos;
            assert(valid());
        }
        iterator             ()                = default;
        iterator             (const iterator&) = default;
        ~iterator            ()                = default;
        iterator& operator = (const iterator&) = default;

        // ----------
        // operator *
        // ----------

        /**
         * dereferences iterator pointer
         */
        reference operator * () const {
            return (*container)[index];
        }           // fix

        // -----------
        // operator ->
        // -----------

        /**
         * a pointer to an element T of this deque
         */
        pointer operator -> () const {
            return &(*container)[index];
        }

        // -----------
        // operator ++
        // -----------

        /**
         * increments interator by 1
         */
        iterator& operator ++ () {
            ++index;
            assert(valid());
            return *this;
        }

        /**
         * increments interator by 1
         */
        iterator operator ++ (int) {
            iterator x = *this;
            ++(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
         * decrements interator by 1
         */
        iterator& operator -- () {
            --index;
            assert(valid());
            return *this;
        }

        /**
         * decrements interator by 1
         */
        iterator operator -- (int) {
            iterator x = *this;
            --(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator +=
        // -----------

        /**
         * increase iterator by d
         */
        iterator& operator += (difference_type d) {
            index += d;
            assert(valid());
            return *this;
        }

        // -----------
        // operator -=
        // -----------

        /**
         * moves interator back d places
         */
        iterator& operator -= (difference_type d) {
            index -= d;
            assert(valid());
            return *this;
        }
    }; // iterator

public:
    // --------------
    // const_iterator
    // --------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        /**
         * see if rhs == lhs
         */
        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return (lhs.index == rhs.index) && (rhs.container == lhs.container);
        } // fix

        /**
        * see if rhs != lhs
        */
        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

        // ----------
        // operator +
        // ----------

        /**
         * moves lhs up by rhs values
         */
        friend const_iterator operator + (const_iterator lhs, difference_type rhs) {
            return lhs += rhs;
        }

        // ----------
        // operator -
        // ----------

        /**
         * moves lhs back by rhs values
         */
        friend const_iterator operator - (const_iterator lhs, difference_type rhs) {
            return lhs -= rhs;
        }

    public:
        // ------
        // usings
        // ------

        // this requires a weaker iterator than the real deque provides
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type        = typename my_deque::value_type;
        using difference_type   = typename my_deque::difference_type;
        using pointer           = typename my_deque::const_pointer;
        using reference         = typename my_deque::const_reference;

    private:
        // ----
        // data
        // ----

        // <your data>
        const my_deque* container;
        size_t index = 0;

    private:
        // -----
        // valid
        // -----

        /**
         * Checks whether this iterator is currently in a valid state or not
         */
        bool valid () const {
            return index >= 0 && (index <= container->size()) && container->valid();
        }

    public:
        // -----------
        // constructor
        // -----------

        /**
         * constructors
         */
        const_iterator (const my_deque* c, size_t pos) {
            container = c;
            index = pos;
            assert(valid());
        }

        const_iterator             ()                      = default;
        const_iterator             (const const_iterator&) = default;
        ~const_iterator            ()                      = default;
        const_iterator& operator = (const const_iterator&) = default;

        // ----------
        // operator *
        // ----------

        /**
         * dereferences current index
         */
        reference operator * () const {
            return (*container)[index];
        }           // fix

        // -----------
        // operator ->
        // -----------

        /**
         * a pointer to an element T of this deque
         */
        pointer operator -> () const {
            return &(*container)[index];
        }

        // -----------
        // operator ++
        // -----------

        /**
         * moves iterator up by 1
         */
        const_iterator& operator ++ () {
            ++index;
            assert(valid());
            return *this;
        }

        /**
         * moves iterator up by 1
         */
        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
        * moves iterator back by 1
        */
        const_iterator& operator -- () {
            --index;
            assert(valid());
            return *this;
        }

        /**
        * moves iterator back by 1
        */
        const_iterator operator -- (int) {
            const_iterator x = *this;
            --(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator +=
        // -----------

        /**
         * moves iterator forward by d places
         */
        const_iterator& operator += (difference_type d) {
            index += d;
            assert(valid());
            return *this;
        }

        // -----------
        // operator -=
        // -----------

        /**
         * moves iterator backwards by d places
         */
        const_iterator& operator -= (difference_type d) {
            index -= d;
            assert(valid());
            return *this;
        }
    }; // const_iterator

public:
    // ------------
    // constructors
    // ------------

    my_deque () {
        outer_begin = _outer.allocate(1);
        *outer_begin = _inner.allocate(inner_array_len);
        outer_end = outer_begin + 1;
        inner_begin = 1;
        inner_end = inner_begin;
        len = 0;
        assert(valid());
    }

    /**
     * size constructor
     */
    explicit my_deque (size_type s) : _inner() {
        len = s;
        if(s < inner_array_len) {
            outer_begin = _outer.allocate(1);
            outer_end = outer_begin + 1;
            *outer_begin =  _inner.allocate(inner_array_len);
            //reach middle
            inner_begin = inner_array_len / 4;
            inner_end = inner_begin + s;
            // use given fill method
            my_uninitialized_fill(_inner, begin(), end(), value_type());
        } else {
            //get how many inner arrays to make
            int inner_arrays = (s / inner_array_len) + 2;
            outer_begin = _outer.allocate(inner_arrays);
            outer_end = outer_begin + inner_arrays;
            //allocate all arrays
            for(int i = 0; i < inner_arrays; ++i) {
                outer_begin[i] = _inner.allocate(inner_array_len);
            }
            //make data start in the middle
            // [......](inner_begin here)[0000000000][0000000000](inner_end here)[......]
            inner_begin = inner_arrays * inner_array_len / 4;
            inner_end = inner_begin + len;
            // use given fill method
            my_uninitialized_fill(_inner, begin(), end(), value_type());
        }
        assert(valid());
    }

    /**
     * size constructor with specified values
     */
    my_deque (size_type s, const_reference v) : _inner() {
        len = s;
        if(s < inner_array_len) {
            outer_begin = _outer.allocate(1);
            outer_end = outer_begin + 1;
            *outer_begin =  _inner.allocate(inner_array_len);
            //reach middle
            inner_begin = inner_array_len / 4;
            inner_end = inner_begin + s;
            // use given fill method
            my_uninitialized_fill(_inner, begin(), end(), v);
        } else {
            //get how many inner arrays to make
            int inner_arrays = (s / inner_array_len) + 2;
            outer_begin = _outer.allocate(inner_arrays);
            outer_end = outer_begin + inner_arrays;
            //allocate all arrays
            for(int i = 0; i < inner_arrays; ++i) {
                outer_begin[i] = _inner.allocate(inner_array_len);
            }
            //make data start in the middle
            // [......](inner_begin here)[0000000000][0000000000](inner_end here)[......]
            inner_begin = inner_arrays * inner_array_len / 4;
            inner_end = inner_begin + len;
            // use given fill method
            my_uninitialized_fill(_inner, begin(), end(), v);
        }
        assert(valid());
    }


    /**
     * size constructor w/ specified values and specified allocator
     */
    my_deque (size_type s, const_reference v, const allocator_type& a) : _inner(a) {
        len = s;
        if(s < inner_array_len) {
            outer_begin = _outer.allocate(1);
            outer_end = outer_begin + 1;
            *outer_begin =  _inner.allocate(inner_array_len);
            //reach middle
            inner_begin = inner_array_len / 4;
            inner_end = inner_begin + s;
            // use given fill method
            my_uninitialized_fill(_inner, inner_begin, inner_end, v);
        } else {
            //get how many inner arrays to make
            int inner_arrays = (s / inner_array_len) + 2;
            outer_begin = _outer.allocate(inner_arrays);
            outer_end = outer_begin + inner_arrays;
            //allocate all arrays
            for(int i = 0; i < inner_arrays; ++i) {
                outer_begin[i] = _inner.allocate(inner_array_len);
            }
            //make data start in the middle
            // [......](inner_begin here)[0000000000][0000000000](inner_end here)[......]
            inner_begin = inner_arrays * inner_array_len / 4;
            inner_end = inner_begin + len;
            // use given fill method
            my_uninitialized_fill(_inner, begin(), end(), v);
        }
        assert(valid());
    }

    /**
     * constructor
     */
    my_deque (std::initializer_list<value_type> rhs)  {
        len = std::distance(rhs.begin(), rhs.end());
        if(len < inner_array_len) {
            outer_begin = _outer.allocate(1);
            outer_end = outer_begin + 1;
            *outer_begin =  _inner.allocate(inner_array_len);
            //reach middle
            inner_begin = inner_array_len / 4;
            inner_end = inner_begin + len;
            // copy elements over
            my_uninitialized_copy(_inner, rhs.begin(), rhs.end(), begin());
        } else {
            //get how many inner arrays to make
            int inner_arrays = (rhs.size / inner_array_len) + 2;
            outer_begin = _outer.allocate(inner_arrays);
            outer_end = outer_begin + inner_arrays;
            //allocate all arrays
            for(int i = 0; i < inner_arrays; ++i) {
                outer_begin[i] = _inner.allocate(inner_array_len);
            }
            //make data start in the middle
            // [......](inner_begin here)[0000000000][0000000000](inner_end here)[......]
            inner_begin = inner_arrays * inner_array_len / 4;
            inner_end = inner_begin + len;
            // copy elements over
            my_uninitialized_copy(_inner, rhs.begin(), rhs.end(), begin());
        }
        assert(valid());
    }

    /**
     * constructor with specified values and allocator
     */
    my_deque (std::initializer_list<value_type> rhs, const allocator_type& a) : _inner(a) {
        len = std::distance(std::begin(rhs), std::end(rhs));
        if(len < inner_array_len) {
            outer_begin = _outer.allocate(1);
            outer_end = outer_begin + 1;
            *outer_begin =  _inner.allocate(inner_array_len);
            //reach middle
            inner_begin = inner_array_len / 4;
            inner_end = inner_begin + len;
            // copy elements over
            my_uninitialized_copy(_inner, rhs.begin(), rhs.end(), begin());
        } else {
            //get how many inner arrays to make
            int inner_arrays = (rhs.size / inner_array_len) + 2;
            outer_begin = _outer.allocate(inner_arrays);
            outer_end = outer_begin + inner_arrays;
            //allocate all arrays
            for(int i = 0; i < inner_arrays; ++i) {
                outer_begin[i] = _inner.allocate(inner_array_len);
            }
            //make data start in the middle
            // [......](inner_begin here)[0000000000][0000000000](inner_end here)[......]
            inner_begin = inner_arrays * inner_array_len / 4;
            inner_end = inner_begin + len;
            // copy elements over
            my_uninitialized_copy(_inner, rhs.begin(), rhs.end(), begin());
        }
        assert(valid());
    }

    /**
    * copy constructor
    */
    my_deque (const my_deque& that) : _inner(that._inner) {
        int inner_arrays = std::distance(that.outer_begin, that.outer_end);
        outer_begin = _outer.allocate(inner_arrays);
        //allocate all arrays
        for(int i = 0; i < inner_arrays; ++i) {
            outer_begin[i] = _inner.allocate(inner_array_len);
        }
        inner_begin = that.inner_begin;
        inner_end = that.inner_end;
        // copy elements over
        my_uninitialized_copy(_inner, that.begin(), that.end(), begin());
        assert(valid());
    }

    // ----------
    // destructor
    // ----------

    /**
     * destroy method
     */
    ~my_deque () {
        my_destroy(_inner, begin(), end());
        for(T** temp = outer_begin; temp < outer_end; ++temp) {
            _inner.deallocate(*temp, inner_array_len);
        }
        int inner_arrays = std::distance(outer_begin, outer_end);
        _outer.deallocate(outer_begin, inner_arrays);
        len = 0;
        assert(valid());
    }

    // ----------
    // operator =
    // ----------

    /**
     * assignment operator
     */
    my_deque& operator = (const my_deque& rhs) {
        my_deque temp(rhs);
        *this = temp;
        assert(valid());
        return *this;
    }

    // -----------
    // operator []
    // -----------

    /**
     * get value at index
     */
    reference operator [] (size_type index) {
        //get offset
        index += inner_begin;
        int outer_index = index / inner_array_len;
        int inner_index = index % inner_array_len;
        return outer_begin[outer_index][inner_index];
    }

    /**
     * get value at index
     */
    const_reference operator [] (size_type index) const {
        return const_cast<my_deque*>(this)->operator[](index);
    }

    // --
    // at
    // --

    /**
     * get value at index
     */
    reference at (size_type index) {
        return (*this)[index];
    }

    /**
    * get value at index
    */
    const_reference at (size_type index) const {
        return const_cast<my_deque*>(this)->at(index);
    }

    // ----
    // back
    // ----

    /**
    * gets last element
    */
    reference back () {
        return *(--end());
    }

    /**
     * gets last element
     */
    const_reference back () const {
        return const_cast<my_deque*>(this)->back();
    }

    // -----
    // begin
    // -----

    /**
     * return iterator that starts at the front
     */
    iterator begin () {
        return iterator(this, 0);
    }

    /**
     * return iterator that starts at the front
     */
    const_iterator begin () const {
        return const_iterator(this, 0);
    }

    // -----
    // clear
    // -----

    /**
     * delete all data
     */
    void clear () {
        resize(0);
        assert(valid());
    }

    // -----
    // empty
    // -----

    /**
     * check to see if size() == 0
     */
    bool empty () const {
        return (size() == 0);
    }

    // ---
    // end
    // ---

    /**
     * return iterator that starts at the end
     */
    iterator end () {
        return iterator(this, size());
    }

    /**
     * return iterator that starts at the front
     */
    const_iterator end () const {
        return const_iterator(this, size());
    }

    // -----
    // erase
    // -----

    /**
     * delete element from deque
     */
    iterator erase (iterator it) {
        if(it == end()) {
            return end();
        }
        //get index
        difference_type index = std::distance(begin(), it);
        bool at_front = (index <= (len / 2));
        _inner.destroy(&*it);
        if(at_front) {
            //moves element to front
            iterator temp = it;
            for(temp; temp != it; --temp) {
                *temp = *(temp - 1);
            }
            ++inner_begin;
        } else {
            //moves element to end
            iterator temp = it;
            for(temp; temp != it; ++temp) {
                *temp = *(temp + 1);
            }
            --inner_end;
        }
        --len;
        assert(valid());
        return it;
    }

    // -----
    // front
    // -----

    /**
     * get first element
     */
    reference front () {
        return (*this)[0];
    }

    /**
     * get first element
     */
    const_reference front () const {
        return const_cast<my_deque*>(this)->front();
    }

    // ------
    // insert
    // ------

    /**
     * insert element into deque
     */
    iterator insert (iterator it, const_reference v) {
        difference_type index = std::distance(begin(), it);
        bool at_front = index <= (len / 2);
        int cap = (std::distance(outer_begin, outer_end) * inner_array_len);
        if(at_front) {
            if(inner_begin <= 0) {
                //reached end resize
                my_deque temp(cap * 2);
                my_uninitialized_copy(_outer, begin(), end(), temp.begin());
                swap(temp);
            }
            //make space for new element
            --inner_begin;
            iterator temp = begin();
            //get to insertion point, moving elements over by 1
            for(temp; temp != it; ++temp) {
                *temp = *(temp + 1);
            }
            //at insertion point
            *temp = v;
        } else {
            if(inner_end >= inner_begin + cap) {
                //reached end, resize and do an early return
                resize(size() + 1, v);
                return it;
            }
            ++inner_end;
            iterator temp = end();
            //get to insertion point
            for(temp; temp != it; --temp) {
                *temp = *(temp - 1);
            }
            *temp = v;
        }
        ++len;
        assert(valid());
        return it;
    }

    // ---
    // pop
    // ---

    /**
     * delete last element
     */
    void pop_back () {
        iterator e = --(end());
        erase(e);
        assert(valid());
    }

    /**
     * delete first element
     */
    void pop_front () {
        erase(begin());
        assert(valid());
    }

    // ----
    // push
    // ----

    /**
     * add element to back
     */
    void push_back (const_reference v) {
        insert(end(), v);
        assert(valid());
    }

    /**
     * add element to front
     */
    void push_front (const_reference v) {
        insert(begin(), v);
        assert(valid());
    }

    // ------
    // resize
    // ------

    /**
     * resize container
     */
    void resize (size_type s) {
        resize(s, value_type());
        assert(valid());
    }

    /**
     *  resize container, pad with (v)s
     */
    void resize (size_type s, const_reference v) {
        if(s == size()) {
            return;
        }
        if(s < size()) {
            //get rid of extra elements
            my_destroy(_inner, begin() + s, end());
            inner_end = inner_begin + s;
        } else {
            int num_to_add = s - size();
            int cap = (std::distance(outer_begin, outer_end) * inner_array_len);
            // too small
            if (size() + num_to_add > cap) {
                my_deque temp(cap * 2);
                my_uninitialized_copy(_outer, begin(), end(), temp.begin());
                swap(temp);
            }
            // pad
            my_uninitialized_fill(_inner, end(), (end() + num_to_add), v);
        }
        assert(valid());
    }

    // ----
    // size
    // ----

    /**
     * return size of deque
     */
    size_type size () const {
        return inner_end - inner_begin;
    }

    // ----
    // swap
    // ----

    /**
     * swaps over all of the members of a deque to another one
     */
    void swap (my_deque& rhs) {
        if (_outer == rhs._outer & _inner == rhs._inner) {
            std::swap(outer_begin, rhs.outer_begin);
            std::swap(outer_end, rhs.outer_end);
            std::swap(inner_begin, rhs.inner_begin);
            std::swap(inner_end, rhs.inner_end);
            std::swap(_outer, rhs._outer);
            std::swap(_inner, rhs._inner);
            std::swap(len, rhs.len);
        } else {
            my_deque x(*this);
            *this = rhs;
            rhs = x;
        }
        assert(valid());
    }
};

#endif // Deque_h